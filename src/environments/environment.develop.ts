// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAT566Zs0qvR6Q5Gl8asIacXqhaBrGhSEU',
    authDomain: 'ng-firebase-dev-8011e.firebaseapp.com',
    databaseURL: 'https://ng-firebase-dev-8011e.firebaseio.com',
    projectId: 'ng-firebase-dev-8011e',
    storageBucket: 'ng-firebase-dev-8011e.appspot.com',
    messagingSenderId: '734483133119'
  }
};
